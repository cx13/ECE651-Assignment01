import java.util.*;

// Chao (Charlio) Xu 
// cx13
// ECE 651 - Software Engineering
// Assignment 01 - Basic Programming in Java

public class Homework01 { 
	
	Primitives prims;
	MoreOnPrims prims2;
	ObjColls obj;
	
	public Homework01(){
		prims = new Primitives(3,4);
		prims2 = new MoreOnPrims(4, 3, 5);
		obj = new ObjColls();
	}
	
	public void printPrims(){
		prims.printResults();
	}
	
	public void printPrims2(){
		prims2.printResults();
	}
	
	public void printColls(){
		obj.manipulate();
		obj.printResults();
	}
	
	public class Primitives {
		
		int a;
		double b;
		char c;
		boolean d;
		
		public Primitives(int x, int y){
			if (x <= y) {
				a = 3*4;
				b = 3.14 + 5;
				c = 'c';
				d = true && !(a > b);
			} else {
				a = 28/7;
				b = 3.14 - 5;
				c = '!';
				d = false || (a != b);
			}
		}
		
		void printResults(){
			System.out.println("Hello World!");
			System.out.printf("a = %d, b = %2.2f, c = %c, d = %b\n", a, b, c, d);
		}
		
	}	
	
	public class MoreOnPrims extends Primitives {
		int[] nums;
		
		public MoreOnPrims(int x, int y, int n){
			super(x, y);
			nums = new int[n];
			
			for (int i=0; i < n; i++) {
				nums[i] = a;
				a += i;
			}
			
		}
		
		public void printResults(){
			super.printResults();
			int i = 0;
			System.out.print("nums are ");
			
			while(i<nums.length){
				System.out.print(nums[i] + " ");
				i++;
			}
			System.out.println();
		}
		
	}
	
	public interface Colls {
		public void manipulate();
		public void printResults();
	}
	
	public class ObjColls implements Colls {
		
		String[] strs;
		ArrayList<Double> myDoubles;
		TreeMap<Integer, String> myMap;
		HashSet<String> mySet;
		
		public ObjColls(){
			initStr();
			initList();
			initMap();
			initSet();
		}
		
		private void initStr(){
			strs = new String[3];
			strs[0] = "Hello World.";
			strs[1] = "Hello Duke.";
			strs[2] = "Hello World.";
		}
		
		private void initList(){
			myDoubles = new ArrayList<Double>();
			for (int i=0; i < 10; i++) {
				myDoubles.add(i, (double)i*(double)i/3 - (double)i);  
			}
		}
		
		private void initMap(){
			myMap = new TreeMap<Integer, String>();
			myMap.put(1, "North Carolina");
			myMap.put(2, "Duke University");
			myMap.put(3, "Software Engineering");
			myMap.put(4, "Software Engineering");
		}
		
		private void initSet(){
			mySet = new HashSet<String>();
			mySet.addAll(myMap.values());
		}
		
		public void manipulate(){
			
			Collections.sort(myDoubles);
			myMap.remove(2);
			mySet.add("Department of Mathematics");
		}
		
		public void printResults(){
			
			System.out.println("String comparison with .equals(): " 
		+ strs[0].equals(strs[1]) + " " + strs[0].equals(strs[2]));
			
			Iterator<Double> it = myDoubles.iterator();
			System.out.println("Print sorted myDoubles list using an iterator:");
			while(it.hasNext()){
				System.out.print(it.next()+" ");
			}
			System.out.println();
			
			System.out.println("Does my map contain Duke University (removed): " 
			+ myMap.containsValue("Duke University"));
			
			System.out.println("What is the size of mySet which should be 4 not 5: " 
			+ mySet.size());
		}
		
		
		
	}
	
	public static void main (String[] args){
		Homework01 obj = new Homework01();
		obj.printPrims();
		obj.printPrims2();
		obj.printColls();
		
		try{
			int a = 10/0;
		} catch(ArithmeticException e){
			System.out.println(e);
		}
	}
}

